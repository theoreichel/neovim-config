
[[_TOC_]]

# General (mostly inherited from vi/vim)
## Navigation
```
Follow link: gx
Go to exact line: 17gg (for line 17)
Go to relative line: 10j (10 lines below)
Go to relative line: 10k (10 lines above)
's' to start 'leap search' which is a sort of 'snake plugin'
CTRL-U scoll upward
CTRL-D scoll downward
zL to scoll right
zH to scoll left
```
mark your code using `ma` for the label 'a' the go back to this position using either `\`` for the exact position or `'` for the line.

## Registers
Yank in a specific register
```
"ayw ((replace) yank word in register a)
"Ayw (add yank word to register a)
"ap (paste register a)
^R a (paste register a in a search field like / or telescope)
```

## Search and replace
https://elanmed.dev/blog/global-find-and-replace-in-neovim

- search with Telescope/Grep - SPACE-G
- open in a QuickFix with CTRL-Q
- execute a search and replace for all file in QuickFix:
```
cdo %s/old_string/new_string/g
```
- save all with :wa or CTRL-S

To search and replace only in the current selection (`v` for "visual"), use
```
:'<,'>s/\%VSEARCH/REPLACE/g
```
where the key element is `\%V` (the rest works as usual)

# Plugin or config related

## LSP
Lint, Formatter and Diagnostic are in neovim core but requires some goodies to work like _mason_, _solargraph_ (ruby), etc.. See the folder _lsp_.
```
gd # go to definition
C-t # come back to original code

gf # show the diagnostic (cop for rubocop)
gi
gl # rubocop diagnostics

K
C-k
```

Use `LspInfo` to manage installed language servers.

## Telescope
Fuzy finder and much more. Bound with `whichkey` to the following shortcuts:
```
<Space>F
<Space>f
<Space>g
```

https://github.com/nvim-telescope/telescope.nvim

## Comment

Use `gcc` for a single line, `gc` for a (visual) block.

## File explorer
Expore the filesystem with `<Space>e`

