#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat << EOF # remove the space between << and EOF, this is due to web plugin issue
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Install archlinux dependencies.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
--nocolor       Dont't print with colors
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  return 0
}

parse_params "$@"
setup_colors

package_list='yq ripgrep alacritty nerd-fonts rubocop npm yarn wl-clipboard go base-devel neovide gitlint stylua rustup
cargo-generate chafa ffmpegthumbnailer'

msg "${RED}The following packages will be installed:${NOFORMAT}"
msg "${package_list}"
msg "${GREEN}Please provide your password to continue${NOFORMAT}"
yay --noconfirm --needed -S ${package_list}

msg "${GREEN}Installing rust dependencies${NOFORMAT}"
# TODO: if there is not already one
rustup default stable
rustup component add rustfmt rust-analyzer llvm-tools

# copy alacritty settings
msg "${RED}Overriding your alacritty settings, ok? (Y/n)${NOFORMAT}"
read override_alacritty
if [[ $override_alacritty == '' ]]
  then override_alacritty = 'y'
fi

if [[ $override_alacritty == [yY] ]]
  then msg "${BLUE}Yeah, overriding${NOFORMAT}"
  mkdir -p ~/.config/alacritty
  cp ./alacritty.toml ~/.config/alacritty/
fi

msg "${BLUE}Adding NeoVim to your local apps.${NOFORMAT}"
mkdir -p ~/.local/share/applications
cp ./nvim.desktop ~/.local/share/applications/

msg "${BLUE}Adding 'nvim-desktop' to your local apps.${NOFORMAT}"
mkdir -p ~/.local/bin
cp ./nvim-desktop ~/.local/bin/
chmod +x ~/.local/bin/nvim-desktop

msg "${BLUE}Adding NeoVide to your local apps.${NOFORMAT}"
mkdir -p ~/.local/share/applications
cp ./neovide.desktop ~/.local/share/applications/

msg "${BLUE}Adding 'neovide-desktop' to your local apps.${NOFORMAT}"
mkdir -p ~/.local/bin
cp ./neovide-desktop ~/.local/bin/
chmod +x ~/.local/bin/neovide-desktop

# msg "${BLUE}Adding neo{vim,vide} keyboard shortcuts (gnome)${NOFORMAT}"
# See add_gnome_shortcuts.sh script
