#!/usr/bin/env bash

SCHEMA="org.gnome.settings-daemon.plugins.media-keys"
KEY="custom-keybindings"
KEY_SCHEMA_BASE="org.gnome.settings-daemon.plugins.media-keys.custom-keybinding"
declare -a SHORTCUTS=("Neovim" "Neovide")

LIST=$(gsettings get ${SCHEMA} ${KEY} | sed "s/[\'\[]//gI" | sed 's/\]//gI')

echo $LIST
IFS=', ' read -r -a LISTA <<< $LIST

for i in ${LISTA[@]}
do
  NAME=$(gsettings get ${KEY_SCHEMA_BASE}:${i} name)
  # Use set and get to add name, command and binding (like "<Ctrl><Alt>b") to add new shortcut
  if [[ "${SHORTCUTS[*]}" =~ "${NAME}" ]]
    then echo $NAME
  else
    echo "${NAME}nope"
  fi
  echo ${SHORTCUTS[1]}
done
