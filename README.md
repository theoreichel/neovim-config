[[_TOC_]]

# Getting started

This setup is particularly adapted for [ruby on rails](https://www.rubyonrails.org), python, lua and JavaScript development at the moment. Installation scripts are provided for [archlinux](https://wwww.archlinux.org).

## Requirements

- [neovim](https://www.neovim.io) > 0.9, `pacman -S neovim` will do just fine
- [archlinux](https://www.archlinux.org) if you plan to use the install script
- work great on debian/ubuntu but there is currently no documentation

## Installation
First clone the repository at the right place.
```
git clone git@gitlab.com:theoreichel/neovim-config.git ~/.config/nvim
```

Then run the install script, as a normal user (not root) and follow the instructions.
```
~/.config/nvim/extra/install-arch.sh
```

The script will install extra dependencies for your own good:
- [alacritty](https://alacritty.org), offers some improvements when using neovim
- [yq](https://github.com/mikefarah/yq), very handy YAML manipulation tool, just like `jq` for JSON
- ripgrep for blazing fast full text search
- nerd-fonts because it looks nice
- node/npm as a dependency for Mason (LSP)
- rubocop as a must-have tool for ruby development
- [NeoVide](https://neovide.dev/) because why not having a fancy ~~Editor~~ IDE!

More dependencies are required so unlock all LSP, Linter and Formatter features. See chapter [LSP](#LSP) for more info.

## Additional dependencies
The below dependencies are yet not installed by the script but might help in some use-cases.

```
sudo luarocks install yaml
```

# Cheat Sheet

Because `whichkey` doesn't tell you all and our organic brains have (sometimes, little) drawbacks.

See [the cheat sheet](CHEATSHEET.md).

# Plugins (non exhaustive, WIP)

## Colorscheme
- [tokyonight-night](https://github.com/folke/tokyonight.nvim)
- [nightfox](https://github.com/EdenEast/nightfox.nvim)

## Packer
Package management. See `:PackerInstall` and _plugins.lua_.

https://github.com/wbthomason/packer.nvim

## Completion
- https://github.com/hrsh7th/nvim-cmp
- https://github.com/windwp/nvim-autopairs

## LSP: Syntax and diagnostic
Mason takes care of installing LSP servers, Linters and Formatters for you. Call it with `:Mason` and install required
packages:
```
 css-lsp cssls
 efm
 html-lsp html
 htmlbeautifier
 jq
 json-lsp jsonls
 lua-language-server lua_ls
 luacheck
 markdownlint
 prettier
 pylint
 pyright
 rubocop
 semgrep
 solargraph
 trivy
 typescript-language-server tsserver
 vue-language-server volar
 yaml-language-server yamlls
 yamlfmt
 yamllint
 yq
```
See Mason for more info.
- https://github.com/williamboman/mason.nvim

Additionally, depending on the project `eslint` might be required. `Eslint` settings are project-specific and are
generally defined in the file `.eslintrc.js` in the project. The `eslint` binary itself must be installed with `yarn`.

Provides a GUI to manage server once installed `LspInfo`.
- https://github.com/neovim/nvim-lspconfig

A diagnostic window:
- https://github.com/folke/trouble.nvim

## Telescope: Fuzy search and much more
https://github.com/nvim-telescope/telescope.nvim

## Comment
https://github.com/numToStr/Comment.nvim

When commenting in a mixed language file, ensure to comment line(s) in the current context.

https://github.com/JoosepAlviste/nvim-ts-context-commentstring

## Syntax color/highlight
- https://github.com/nvim-treesitter/nvim-treesitter
- https://github.com/RRethy/vim-illuminate

## Git
- https://github.com/lewis6991/gitsigns.nvim
- https://github.com/TimUntersberger/neogit

## nvim-tree: a file explorer
https://github.com/nvim-tree/nvim-tree.lua

## indent
https://github.com/lukas-reineke/indent-blankline.nvim

## Barbar: Buffer bar, tabs
TODO

## Missing plugins
Justify all used plugins

# Integration

## Linux (Archlinux, Ubuntu)
If you like to separate Neovim windows from your Terminal windows (and you are not already using stterm for this), you run `neovim-desktop` (installed by the script, see its content) or put it in yous keyboard shortcuts (session). See _nvim.desktop_ file in the root directory of this project, this is what replace the `neovim` app (GUI) in your session.
```
alacritty --class nvim -T neovim -e zsh -c "source ~/.zshrc && nvim"
```
You can easily adjust this command to fit your needs in other contexts.

If you are real, give _neovide_ a try with respectively:

for your keyboard shortcut:
```
zsh -c "source ~/.zshrc && neovide --multigrid --notabs --frame none --geometry 135x60"
```
or directly in a shell: `neovide-desktop WHATEVER_FOLDER_OR_FILE`

# Credits
- Inspired by https://www.youtube.com/@chrisatmachine

# Further readings
- https://alpha2phi.medium.com/learn-neovim-the-practical-way-8818fcf4830f

