return {
	settings = {
		Lua = {
			diagnostics = {
				globals = { "vim", "describe", "it" },
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
			completion = {
				autoRequire = true,
				callSnippet = "Replace",
				displayContext = 1,
			},
			format = {
				defaultConfig = {
					indent_style = "space",
					indent_size = "2",
				},
			},
			hint = {
				enable = true,
				arrayIndex = "Enable",
				setType = true,
			},
			semantic = {
				enable = true,
			},
		},
	},
}
