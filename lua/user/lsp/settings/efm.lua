-- This doesn't seem to work
return {
  settings = {
    rootMarkers = { ".git/" },
    languages = {
      slim = {
        {
          lintCommand = "slim-lint",
          lintStdin = true,
          lintFormats ={ '%f:%l [%c] %m' },
        }
      }
    }
  },
  filetypes = { 'slim' }
}
