return {
  settings = {
    tsserver = {
      format = { enable = true },
    },
    eslint = {
      enable = true,
      format = { enable = true },
      packageManager = "npm",
      autoFixOnSave = true,
      codeActionsOnSave = {
        mode = "all",
        rules = { "!debugger", "!no-only-tests/*" },
      },
      lintTask = {
        enable = true,
      },
    },
    -- root_dir = function() return vim.loop.cwd() end
  }
}
