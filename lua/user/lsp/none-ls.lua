return function()
  null_ls = require("null-ls")

  null_ls.setup {
    sources = {
      null_ls.builtins.code_actions.gitsigns,
      null_ls.builtins.completion.spell,
      null_ls.builtins.completion.luasnip,
      null_ls.builtins.diagnostics.dotenv_linter,
      null_ls.builtins.diagnostics.erb_lint,
      null_ls.builtins.diagnostics.gitlint,
      null_ls.builtins.diagnostics.markdownlint,
      null_ls.builtins.diagnostics.semgrep.with({
        args = { "--config", "auto", "-q", "--json", "--timeout", "0", "$FILENAME" },
        timeout = 15000,
      }),
      null_ls.builtins.diagnostics.tidy,
      null_ls.builtins.diagnostics.trivy,
      null_ls.builtins.diagnostics.yamllint,
      null_ls.builtins.diagnostics.zsh,
      -- null_ls.builtins.formatting.beautysh,
      null_ls.builtins.formatting.erb_format,
      null_ls.builtins.formatting.erb_lint,
      -- null_ls.builtins.formatting.htmlbeautifier,
      -- null_ls.builtins.formatting.jq,
      null_ls.builtins.formatting.markdownlint,
      null_ls.builtins.formatting.nginx_beautifier,
      null_ls.builtins.formatting.pg_format,
      null_ls.builtins.formatting.remark,
      null_ls.builtins.formatting.sqlfluff,
      null_ls.builtins.formatting.tidy,
      -- null_ls.builtins.formatting.xq,
      -- null_ls.builtins.formatting.yq,
      null_ls.builtins.hover.dictionary,
      null_ls.builtins.hover.printenv,
    },
    filetypes = { 'ruby' }
  }
end
