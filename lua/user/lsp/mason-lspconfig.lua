return function()
  local lspconfig = require("lspconfig")
  local cmp_nvim_lsp = require("cmp_nvim_lsp")
  local mason_lspconfig = require("mason-lspconfig")

  local servers = {
    "solargraph",
    "tsserver",
    "cssls",
    "html",
    "pyright",
    "jsonls",
    "yamlls",
    "lua_ls",
    "efm",
    "rust_analyzer",
    "marksman",
    "eslint",
    "clangd"
  }

  mason_lspconfig.setup({
    ensure_installed = servers,
    automatic_installation = true,
  })

  local function lsp_keymaps(bufnr)
    local opts = { noremap = true, silent = true }
    local keymap = vim.api.nvim_buf_set_keymap
    keymap(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
    keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
    keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
    keymap(bufnr, "n", "gI", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
    keymap(bufnr, "n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
    keymap(bufnr, "n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
  end

  for _, server in pairs(servers) do
    local opts = {}
    opts.on_attach = function(client, bufnr)
      -- if client.name == "tsserver" then
      --   client.server_capabilities.documentFormattingProvider = false
      -- end

      if client.name == "efm" then
        client.server_capabilities.documentFormattingProvider = false
      end

      if client.name == "eslint" then
        client.resolved_capabilities.document_formatting = true
        client.resolved_capabilities.document_range_formatting = true
      end

      lsp_keymaps(bufnr)
      local status_ok, illuminate = pcall(require, "illuminate")
      if not status_ok then
        vim.notify("Unable to load illuminate", vim.log.levels.WARN)
        return
      end
      illuminate.on_attach(client)
    end

    opts.capabilities = vim.lsp.protocol.make_client_capabilities()
    opts.capabilities.textDocument.completion.completionItem.snippetSupport = true
    opts.capabilities = cmp_nvim_lsp.default_capabilities(opts.capabilities)

    server = vim.split(server, "@")[1]

    local require_ok, conf_opts = pcall(require, "user.lsp.settings." .. server)
    if require_ok then
      opts = vim.tbl_deep_extend("force", conf_opts, opts)
    else
      vim.notify("Unable to load user.lsp.settings." .. server, vim.log.levels.WARN)
    end

    lspconfig[server].setup(opts)
  end
end
