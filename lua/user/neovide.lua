if vim.g.neovide then
  -- vim.o.guifont = "Agave_Nerd_Font_Mono:h17"
  -- vim.o.guifont = "Mononoki_Nerd_Font:h17:x20"
  vim.o.guifont = "monospace:h15:w-0.5:#e-subpixelantialias:#h-slight"
  -- vim.o.guifont = "Hack_Nerd_Font:h15"
  vim.g.neovide_scale_factor = 1.0
  vim.opt.linespace = 0

  -- g:neovide_transparency should be 0 if you want to unify transparency of content and title bar.
  -- vim.g.neovide_transparency = 0.95
  -- vim.g.transparency = 0.95
  vim.g.neovide_scroll_animation_length = 0.1
  vim.g.neovide_scroll_animation_far_lines = 5
  vim.g.neovide_hide_mouse_when_typing = true

  vim.g.neovide_padding_top = 0
  vim.g.neovide_padding_bottom = 0
  vim.g.neovide_padding_right = 0
  vim.g.neovide_padding_left = 5

  -- railgun, pixiedust, wireframe
  vim.g.neovide_cursor_vfx_mode = "pixiedust"
  vim.g.neovide_cursor_vfx_opacity = 0
  vim.g.neovide_cursor_vfx_particle_lifetime = 0
  vim.g.neovide_cursor_vfx_particle_density = 0.0
  vim.g.neovide_cursor_vfx_particle_speed = 0.0
  vim.g.neovide_cursor_vfx_particle_phase = 1.5 -- only railgun
  vim.g.neovide_cursor_vfx_particle_curl = 1.0  -- only railgun

  -- vim.g.neovide_floating_blur_amount_x = 1.0
  -- vim.g.neovide_floating_blur_amount_y = 1.0

  vim.g.neovide_theme = 'auto'
  vim.g.neovide_fullscreen = false

  vim.g.neovide_remember_window_size = true

  vim.g.neovide_profiler = false
  vim.g.neovide_cursor_animation_length = 0.13
  vim.g.neovide_cursor_trail_size = 0.2
  vim.g.neovide_cursor_antialiasing = true
  vim.g.neovide_cursor_animate_in_insert_mode = true
end

