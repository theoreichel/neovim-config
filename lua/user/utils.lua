local yaml_ok, yaml_nvim = pcall(require, "yaml_nvim")
if not yaml_ok then
  vim.notify("Unable to load yaml_nvim", vim.log.levels.WARN)
  return
end

-- ----------------- --
-- RoRi18nRenamePath --
-- ----------------- --

-- TODO: rewrite using i18n-tasks mv
-- TODO: add a function to jump from a translation anywhere in code to the right line in the default locale (rewrite everything in ruby?)
-- TODO: jump from one language file to another at the same line

-- Regex to match the translation object in project
local function translationMatcher(str)
  return 't([\\"\']' .. str .. '[\\"\']'
end

-- Build a complex command to retrieve output from 'yq', mainly. The issue lies in context: data is piped from the
-- buffer and should not be accessed through the filesystem where the file might be outdated. We don't want to save
-- at this stage.
local function escapeCmd(cmd)
  -- Example of working escape sequences:
  -- local redirect = 'redir @">|exe(\'w !yq e ".en.admin.attachment" -\')|redir END' --works
  -- local redirect = [[redir @">|exe('w !yq e ''.en.admin.attachment | select(tag == "\!\!map")'' -')|redir END]] --works

  -- This illegible chain of chars does the following: packs the 'cmd' in a 'redir' to save it to a register
  local redirect = 'redir @">|exe(\'' .. cmd .. '\')|redir END'
  vim.cmd(redirect)
  -- Get data from the previously set register using 'redir'
  local output = vim.fn.getreg('"')
  -- trim
  output = output:gsub("^%s+", ""):gsub("%s+$", "")
  return output
end

local function movePath(old_path, new_path)
  local tag = escapeCmd([[silent w !yq e ''.]] .. new_path .. [[ | type'' -]])
  local operator = ' = ' -- if tag == nil or any type that cannot/shoudn't be merged.
  if tag == '!!map' or tag == '!!seq' then operator = ' *= ' end

  vim.cmd("silent %!yq e '." .. new_path .. operator .. "." .. old_path .. "| del(." .. old_path .. "), sort_keys(..)' -")
  vim.cmd("silent %!yq e 'del(.. | select(tag == \"\\!\\!map\" and length == 0))' -")
end

local function loadAllLocales(dir, original_filename)
  local locales = {}
  local handle = assert(vim.loop.fs_scandir(dir))

  local current_locale = original_filename:match("([^%.]+)%.yml")
  local basename = original_filename:match("(.*)%." .. current_locale .. "%.yml")
  local filename_wildcard = basename .. "%...%.yml"

  while true do
    local name, type = vim.loop.fs_scandir_next(handle)
    if name == nil then
      break
    end
    if type == 'file' and name:match(filename_wildcard) then
      vim.cmd('edit ' .. dir .. "/" .. name)
      locales[name:match("([^%.]+)%.yml")] = vim.api.nvim_get_current_buf()
    end
  end

  return locales
end

-- Replace translations path in project's files
local function updateFiles(locale, buffer_id, old_path, new_path)
  vim.api.nvim_set_current_buf(buffer_id)

  local node = escapeCmd([[silent w !yq e ''.]] .. locale .. "." .. old_path .. [[ | select(tag == "\!\!map")'' -]])

  if node == "" then
    -- leaf (string)
    print("Updating " .. old_path .. " to " .. new_path)
    vim.cmd([[silent!grep -r "]] .. translationMatcher(old_path) .. [[" **/*.{rb,slim,sass,rake,erb,jbuilder}]])
    vim.cmd('silent!cdo %s/' .. translationMatcher(old_path) .. '/t(\'' .. new_path .. '\'/g')
  else
    -- branch (map)
    local keys = escapeCmd([[silent w !yq e ''.]] .. locale .. "." .. old_path .. [[ | keys'' -]])
    for key in keys:gmatch("([^-%s]+)") do
      updateFiles(locale, buffer_id, old_path .. "." .. key, new_path .. "." .. key)
    end
  end
end

-- TODO: Build a nice UI instead of the text prompt
vim.api.nvim_create_user_command(
  'RoRi18nRenamePath',
  function()
    if vim.bo.filetype ~= 'yaml' then
      print("This command can only be called on a YAML file. See /config/locales/*.yml in your rails project. ")
      return
    end

    yaml_nvim.yank_key() -- default to unamed register '"'
    local yanked_key = vim.fn.getreg('"')

    local current_line_number = vim.api.nvim__buf_stats(0).current_lnum
    local current_locale, current_path = string.match(yanked_key, "(..)%.(.*)")
    local new_path = ""

    vim.ui.input({ prompt = 'New translation path: ', default = current_path }, function(form_input)
      new_path = form_input
      print("\n")
    end)

    -- TODO: Validate inputs
    if new_path == nil or new_path == "" or new_path == current_path then return end

    -- Move the translation path in the current file
    local source_buffer_id = vim.api.nvim_get_current_buf()
    local abs_file_path = vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
    local filename = abs_file_path:match("^.+/(.+)$")
    local abs_directory = vim.fn.expand("%:p:h")
    local locales = loadAllLocales(abs_directory, filename)

    -- Update project's code
    updateFiles(current_locale, source_buffer_id, current_path, new_path)

    for locale, buffer_id in pairs(locales) do
      -- TODO: it is probably better to pass the buffer_id to each commands of replaceBufferContent and ensure
      -- commands are run on the right buffer instead of switching visually each translation files.

      -- Switch to the corresponding locale buffer
      vim.api.nvim_set_current_buf(buffer_id)

      -- Update the i18n file (buffer)
      movePath(locale .. "." .. current_path, locale .. "." .. new_path)
    end

    -- Return to original buffer, original cursor position
    vim.api.nvim_set_current_buf(source_buffer_id)
    vim.cmd(":" .. current_line_number)
  end,
  { bang = false, desc = "Rename the translation path found in a RoR YAML file, project-wide." }
)

vim.api.nvim_create_user_command(
  'RubocopDisableCop',
  function()
    local current_line_number = vim.api.nvim__buf_stats(0).current_lnum
    for index, value in ipairs(vim.diagnostic.get(0)) do
      if value["lnum"] == current_line_number - 1 then
        -- TODO use tree sitter to place comments around the instruction block
        -- insert above
        vim.cmd("norm O# rubocop:disable " .. value["code"])
        -- return to original line
        vim.cmd("norm j")
        -- insert above
        vim.cmd("norm o# rubocop:enable " .. value["code"])
      end
    end
  end,
  { bang = false, desc = "Disable current raised cop." }
)

-- vim.api.nvim_create_user_command(
--   'RoRi18nFindReference',
--   function()
--     if vim.bo.filetype ~= 'yaml' then
--       print("This command can only be called on a YAML file. See /config/locales/*.yml in your rails project. ")
--       return
--     end
--
--     yaml_nvim.yank_key() -- default to unamed register '"'
--     local yanked_key = vim.fn.getreg('"')
--
--     local current_line_number = vim.api.nvim__buf_stats(0).current_lnum
--
--     -- Move the translation path in the current file
--     local source_buffer_id = vim.api.nvim_get_current_buf()
--     local abs_file_path = vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
--     local filename = abs_file_path:match("^.+/(.+)$")
--     local abs_directory = vim.fn.expand("%:p:h")
--     local locales = loadAllLocales(abs_directory, filename)
--
--     -- Return to original buffer, original cursor position
--     vim.api.nvim_set_current_buf(source_buffer_id)
--     vim.cmd(":" .. current_line_number)
--   end,
--   { bang = false, desc = "Find references to this translation path, project-wide." }
-- )

-- GitUI
vim.api.nvim_create_user_command("GitUIToggle", function()
  local fterm = require("FTerm")
  local gitui = fterm:new({
    ft = 'fterm_gitui', -- You can also override the default filetype, if you want
    cmd = "gitui",
    dimensions = {
      height = 0.9,
      width = 0.9
    }
  })
  gitui:toggle()
end, { bang = true })
