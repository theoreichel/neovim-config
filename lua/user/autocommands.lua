-- TODO: Move autocommands to file requiring it instead of splitting them here. Most of them are not "generic".
vim.cmd([[
  augroup _general_settings
    autocmd!
    autocmd FileType qf,help,man,lspinfo nnoremap <silent> <buffer> q :close<CR>
    autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Visual', timeout = 200})
    autocmd BufWinEnter * :set formatoptions-=cro
    autocmd FileType qf set nobuflisted
  augroup end

  augroup _git
    autocmd!
    autocmd FileType gitcommit setlocal wrap
    autocmd FileType gitcommit setlocal spell
  augroup end

  augroup _markdown
    autocmd!
    autocmd FileType markdown setlocal wrap
    autocmd FileType markdown setlocal spell
  augroup end

  augroup _auto_resize
    autocmd!
    autocmd VimResized * tabdo wincmd =
  augroup end

  augroup _alpha
    autocmd!
    autocmd User AlphaReady set showtabline=0 | autocmd BufUnload <buffer> set showtabline=2
  augroup end
]])

-- Autocommand that reloads neovim whenever you save the plugins.lua file
-- autocmd BufWritePost plugins.lua source <afile> | PackerSync
-- vim.cmd([[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile>
--   augroup end
-- ]])

-- TODO: convert above autocmd to neovim flavor?
-- example
-- local augroup = vim.api.nvim_create_augroup('highlight_cmds', {clear = true})
--
-- vim.api.nvim_create_autocmd('ColorScheme', {
--   pattern = 'rubber',
--   group = augroup,
--   desc = 'Change string highlight',
--   callback = function()
--     vim.api.nvim_set_hl(0, 'String', {fg = '#FFEB95'})
--   end
-- })

-- Clear trailing spaces when saving
local save_callbacks = vim.api.nvim_create_augroup("_save_callbacks", { clear = true })
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  pattern = { "*" },
  group = save_callbacks,
  desc = "Remove trailing spaces on save",
  command = [[%s/\s\+$//e]],
})

-- Restore cursor position: https://github.com/neovim/neovim/issues/16339#issuecomment-1457394370
vim.api.nvim_create_autocmd("BufRead", {
  callback = function(opts)
    vim.api.nvim_create_autocmd("BufWinEnter", {
      once = true,
      buffer = opts.buf,
      callback = function()
        local ft = vim.bo[opts.buf].filetype
        local last_known_line = vim.api.nvim_buf_get_mark(opts.buf, '"')[1]
        if
            not (ft:match("commit") and ft:match("rebase"))
            and last_known_line > 1
            and last_known_line <= vim.api.nvim_buf_line_count(opts.buf)
        then
          vim.api.nvim_feedkeys([[g`"]], "nx", false)
        end
      end,
    })
  end,
})
