return {
  "nvim-lua/plenary.nvim",
  {
    "windwp/nvim-autopairs",
    opts = require("user.config.autopairs")
  },
  {
    "numToStr/Comment.nvim",
    opts = require("user.config.comment"),
    lazy = false
  },
  "JoosepAlviste/nvim-ts-context-commentstring",

  -- GUI
  {
    'nvim-tree/nvim-tree.lua',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    opts = require("user.config.nvim-tree"),
    lazy = false
  },
  {
    "romgrk/barbar.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
      "lewis6991/gitsigns.nvim"
    },
    init = function() vim.g.barbar_auto_setup = false end,
    opts = require("user.config.barbar"),
    lazy = false
  },
  "moll/vim-bbye",
  {
    "nvim-lualine/lualine.nvim",
    opts = require("user.config.lualine"),
    lazy = false
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    init = function()
      vim.opt.list = true
      -- vim.opt.listchars:append "space:⋅"
      vim.opt.listchars:append("eol:↴")
    end,
    opts = require("user.config.indentline")
  },
  {
    "norcalli/nvim-colorizer.lua",
    event = "BufEnter",
    opts = { "*" }
  },
  {
    "rcarriga/nvim-notify", -- better notifications
    config = function() vim.notify = require("notify") end,
    lazy = false
  },

  -- Help
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    config = require("user.config.whichkey")
  },

  -- Colorschemes
  {
    "EdenEast/nightfox.nvim",
    config = require("user.config.colorscheme")
  },

  -- Syntax highlighting
  --  { "slim-template/vim-slim" } -- horribly slow
  "onemanstartup/vim-slim",
  --  { "davydovanton/vim-html2slim" } -- requires neovim-ruby-host to work
  "RRethy/vim-illuminate",

  -- Cmp
  {
    "hrsh7th/nvim-cmp",
    config = require("user.config.cmp"),
    dependencies = {
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "saadparwaiz1/cmp_luasnip",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lua"
    }
  },

  -- Snippets
  "L3MON4D3/LuaSnip",
  "rafamadriz/friendly-snippets",
  {
    "danymat/neogen",
    opts = require("user.config.neogen")
  },

  -- LSP, order matter
  {
    "neovim/nvim-lspconfig",
    config = require("user.lsp.base"),
    dependencies = {
      {
        "williamboman/mason.nvim",
        opts = {
          ui = {
            border = "none",
            icons = {
              package_installed = "",
              package_pending = "",
              package_uninstalled = "",
            },
          },
          log_level = vim.log.levels.INFO,
          max_concurrent_installers = 4,
        },
        priority = 40
      },
      {
        "williamboman/mason-lspconfig.nvim",
        config = require("user.lsp.mason-lspconfig"),
        dependencies = {
          "williamboman/mason.nvim"
        }
      },
      {
        "nvimtools/none-ls.nvim",
        config = require("user.lsp.none-ls"),
        enabled = false
      },
      {
        "simrat39/rust-tools.nvim",
        opts = require("user.lsp.additions.rust")
      },
      "mfussenegger/nvim-dap"
    },
    lazy = false
  },
  {
    "folke/trouble.nvim",
    opts = require("user.config.trouble"),
    dependencies = "nvim-tree/nvim-web-devicons"
  },

  -- Telescope and Navigation
  {
    "goolord/alpha-nvim",
    config = require("user.config.alpha"),
    lazy = false
  },
  {
    "nvim-telescope/telescope.nvim",
    config = require("user.config.telescope"),
    dependencies = {
      "nvim-telescope/telescope-live-grep-args.nvim"
    }
  },
  'stevearc/dressing.nvim',
  {
    "ggandor/leap.nvim",
    opts = require("user.config.leap"),
    init = function()
      require("leap").add_default_mappings()
    end,
    dependencies = "tpope/vim-repeat"
  },
  {
    "coffebar/neovim-project",
    init = function()
      -- enable saving the state of plugins in the session
      -- save global variables that start with an uppercase letter and contain at least one lowercase letter.
      vim.opt.sessionoptions:append("globals")
    end,
    opts = require("user.config.project-manager"),
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      {
        "nvim-telescope/telescope.nvim",
        tag = "0.1.4"
      },
      { "Shatur/neovim-session-manager" }
    }
  },

  -- Treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    opts = require("user.config.treesitter")
  },
  {
    "cuducos/yaml.nvim",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim"
    }
  },

  -- Git
  {
    "TimUntersberger/neogit",
    tag = 'v0.0.1',
    opts = {
      integrations = {
        telescope = true,
        diffview = true
      }
    },
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
      {
        'sindrets/diffview.nvim',
        opts = {
          enhanced_diff_hl = true
        }
      }
    },
    lazy = false
  },
  {
    "lewis6991/gitsigns.nvim",
    opts = require("user.config.gitsigns"),
    tag = "v0.6"
  },

  -- misc
  "numToStr/FTerm.nvim",

  -- Language specific plugins
  {
    "theoo/rails-minitest.nvim",
    dir = "~/Code/nvim-plugins/rails-minitest.nvim",
    dev = true,
    dependencies = { "numToStr/Fterm.nvim" },
    opts = require("user.config.rails-minitest")
  }
}
