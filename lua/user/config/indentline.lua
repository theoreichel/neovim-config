
return {
  enabled = true,
  scope = {
    enabled = true,
    show_start = false,
    show_end = false,
    highlight = { "Keyword" },
  },
  indent = { char = "│" },
  exclude = {
    buftypes = {
      "terminal",
      "nofile",
      "quickfix",
      "prompt",
      "help",
      "startify",
      "dashboard",
      "packer",
      "neogitstatus",
      "NvimTree",
      "Trouble",
    },
  },
}
