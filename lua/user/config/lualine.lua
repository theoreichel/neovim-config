local hide_in_width = function()
  return vim.fn.winwidth(0) > 80
end

local diagnostics = {
  "diagnostics",
  sources = { "nvim_diagnostic" },
  sections = { "error", "warn" },
  symbols = { error = " ", warn = " " },
  colored = false,
  update_in_insert = false,
  always_visible = true,
}

local diff = {
  "diff",
  colored = false,
  symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
  cond = hide_in_width,
}

local mode = {
  "mode",
}

local filetype = {
  "filetype",
  icons_enabled = true,
  colored = false,
  -- icon = nil,
}

local branch = {
  "branch",
  icons_enabled = true,
  icon = "",
}

local searchcount = {
  "searchcount",
  draw_empty = false,
}

local location = {
  "location",
  padding = 0,
}

local selectioncount = {
  "selectioncount",
  fmt = function(str)
    if str == "" then
      return "·"
    else
      return "[" .. str .. "]"
    end
  end,
}

-- cool function for progress
local progress = function()
  local current_line = vim.fn.line(".")
  local total_lines = vim.fn.line("$")
  local chars = { "__", "▁▁", "▂▂", "▃▃", "▄▄", "▅▅", "▆▆", "▇▇", "██" }
  local line_ratio = current_line / total_lines
  local index = math.ceil(line_ratio * #chars)
  return chars[index]
end

local spaces = function()
  return "spaces: " .. vim.api.nvim_buf_get_option(0, "shiftwidth")
end

local filename = {
  "filename",
  file_statue = true,
  path = 1,
  cond = hide_in_width,
}

return {
  options = {
    icons_enabled = true,
    theme = "auto",
    component_separators = { left = "", right = "·" },
    section_separators = { left = "", right = "" },
    disabled_filetypes = { "alpha", "dashboard", "Outline" },
    always_divide_middle = true,
    globalstatus = true,
  },
  sections = {
    lualine_a = { mode },
    lualine_b = { branch, diagnostics },
    lualine_c = { filename },
    lualine_x = { searchcount, "filesize", diff, spaces, "encoding", filetype },
    lualine_y = { location, selectioncount },
    lualine_z = { progress },
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { "filename" },
    lualine_x = { "location" },
    lualine_y = {},
    lualine_z = {},
  },
  tabline = {},   -- incompatible with barbar
  extensions = { "quickfix", "nvim-tree", "trouble" },
}
