return function()
  local colorscheme = "nightfox"

  local Shade = require("nightfox.lib.shade")

  require(colorscheme).setup({
    options = {
      -- Compiled file's destination location
      compile_path = vim.fn.stdpath("cache") .. "/nightfox",
      compile_file_suffix = "_compiled", -- Compiled file suffix
      transparent = false,               -- Disable setting background
      terminal_colors = true,            -- Set terminal colors (vim.g.terminal_color_*) used in `:terminal`
      dim_inactive = true,               -- Non focused panes set to alternative background
      module_default = true,             -- Default enable value for modules
      colorblind = {
        enable = false,                  -- Enable colorblind support
        simulate_only = false,           -- Only show simulated colorblind colors and not diff shifted
        severity = {
          protan = 0,                    -- Severity [0,1] for protan (red)
          deutan = 0,                    -- Severity [0,1] for deutan (green)
          tritan = 0,                    -- Severity [0,1] for tritan (blue)
        },
      },
      styles = {             -- Style to be applied to different syntax groups
        comments = "italic", -- Value is any valid attr-list value `:help attr-list`
        conditionals = "NONE",
        constants = "NONE",
        functions = "NONE",
        keywords = "NONE",
        numbers = "NONE",
        operators = "NONE",
        strings = "NONE",
        types = "NONE",
        variables = "NONE",
      },
      inverse = { -- Inverse highlight for different types
        match_paren = false,
        visual = false,
        search = false,
      },
      modules = { -- List of various plugins and additional options
        -- ...
      },
    },
    palettes = {
      nightfox = {
        -- Inspired from
        -- https://github.com/xy2z/SublimeTwilight/blob/master/Twilight%20Theme%20Colors.yaml
        -- Dark brown: #9B703F
        -- Orange/brown (keyword): #CDA869
        -- Green (strings): #8F9D6A
        -- Blue (variable): #7587A6
        -- Red (constant): #CF6A4C
        -- Yellow (storage): #F9EE98

        -- Adjusted with
        -- https://colorkit.co/palette/9B859D-7587a6-8f9d6a-cf6a4c-9B703F-cda869-f9ee98-97c8ed-ed97c8/
        -- #9B859D, #7587a6, #8f9d6a, #cf6a4c, #9B703F, #cda869, #f9ee98, #97c8ed, #ed97c8

        black = Shade.new("#393b44", 0.15, -0.15),
        red = Shade.new("#cf6a4c", 0.15, -0.15),
        green = Shade.new("#8f9d6a", 0.10, -0.15),
        yellow = Shade.new("#f9ee98", 0.15, -0.15),
        blue = Shade.new("#7587a6", 0.15, -0.15),
        magenta = Shade.new("#9B859D", 0.30, -0.15),
        cyan = Shade.new("#97c8ed", 0.15, -0.15),
        white = Shade.new("#dfdfe0", 0.15, -0.15),
        orange = Shade.new("#cda869", 0.15, -0.15),
        pink = Shade.new("#ed97c8", 0.15, -0.15),
        brown = Shade.new("#9B703F", 0.15, -0.15),

        comment = "#65686c",

        -- https://colorkit.co/palette/1F1F1F-1c1c1c-191919-151515-080D12-464a4e-65686c-848689-c2c3c4/
        -- #1F1F1F, #1c1c1c, #191919, #151515, #080D12, #464a4e, #65686c, #848689, #c2c3c4

        bg0 = "#151515",  -- Dark bg (status line and float)
        bg1 = "#191919",  -- Default bg
        bg2 = "#1c1c1c",  -- Lighter bg (colorcolm folds)
        bg3 = "#282828",  -- Lighter bg (cursor line)
        bg4 = "#464a4e",  -- Conceal, border fg

        fg0 = "#d6d6d7",  -- Lighter fg
        fg1 = "#cdcecf",  -- Default fg
        fg2 = "#aeafb0",  -- Darker fg (status line)
        fg3 = "#848689",  -- Darker fg (line numbers, fold colums)

        sel0 = "#2b3b51", -- Popup bg, visual selection bg
        sel1 = "#3c5372", -- Popup sel bg, search bg
      },
    },
    specs = {
      nightfox = {
        syntax = {
          bracket = "fg2",             -- Brackets and Punctuation
          builtin0 = "red.base",       -- Builtin variable
          builtin1 = "cyan.bright",    -- Builtin type
          builtin2 = "orange.bright",  -- Builtin const
          builtin3 = "red.bright",     -- Not used
          comment = "comment",         -- Comment
          conditional = "orange.base", -- Conditional and loop
          const = "red.bright",      -- Constants, imports and booleans
          dep = "fg3",                 -- Deprecated
          field = "blue.dim",          -- Field
          func = "brown.bright",       -- Functions and Titles
          ident = "cyan.base",         -- Identifiers
          keyword = "orange.bright",   -- Keywords
          number = "red.base",       -- Numbers
          operator = "fg2",            -- Operators
          preproc = "orange.bright",        -- PreProc -- fix
          regex = "yellow.bright",     -- Regex
          statement = "magenta.base",  -- Statements
          string = "green.base",       -- Strings
          type = "magenta.base",       -- Types
          variable = "white.base",     -- Variables
        },
      },
    },
    groups = {},
  })


  -- Highlight trailing spaces
  vim.cmd("highlight TrailingWhitespaces ctermbg=red guibg=red")
  vim.cmd("match TrailingWhitespaces /\\s\\+$/")
end
