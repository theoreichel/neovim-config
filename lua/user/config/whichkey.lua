return function()
  which_key = require("which-key")

  local setup = {
    plugins = {
      marks = true,     -- shows a list of your marks on ' and `
      registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
      spelling = {
        enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
        suggestions = 20, -- how many suggestions should be shown in the list?
      },
      -- the presets plugin, adds help for a bunch of default keybindings in Neovim
      -- No actual key bindings are created
      presets = {
        operators = true,  -- adds help for operators like d, y, ... and registers them for motion / text object completion
        motions = true,    -- adds help for motions
        text_objects = true, -- help for text objects triggered after entering an operator
        windows = true,    -- default bindings on <c-w>
        nav = true,        -- misc bindings to work with windows
        z = true,          -- bindings for folds, spelling and others prefixed with z
        g = true,          -- bindings for prefixed with g
      },
    },
    -- add operators that will trigger motion and text object completion
    -- to enable all native operators, set the preset / operators plugin above
    -- operators = { gc = "Comments" },
    key_labels = {
      -- override the label used to display some keys. It doesn't effect WK in any other way.
      -- For example:
      -- ["<space>"] = "SPC",
      -- ["<cr>"] = "RET",
      -- ["<tab>"] = "TAB",
    },
    icons = {
      breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
      separator = "➜", -- symbol used between a key and it's label
      group = "+", -- symbol prepended to a group
    },
    popup_mappings = {
      scroll_down = "<c-d>", -- binding to scroll down inside the popup
      scroll_up = "<c-u>", -- binding to scroll up inside the popup
    },
    window = {
      border = "rounded",     -- none, single, double, shadow
      position = "bottom",    -- bottom, top
      margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
      padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
      winblend = 10,
    },
    layout = {
      height = { min = 4, max = 25 },                                           -- min and max height of the columns
      width = { min = 20, max = 50 },                                           -- min and max width of the columns
      spacing = 3,                                                              -- spacing between columns
      align = "left",                                                           -- align columns left, center or right
    },
    ignore_missing = false,                                                     -- enable this to hide mappings for which you didn't specify a label
    hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
    show_help = true,                                                           -- show help message on the command line when the popup is visible
    show_keys = true,                                                           -- show help message on the command line when the popup is visible
    triggers = "auto",                                                          -- automatically setup triggers
    -- triggers = {"<leader>"} -- or specify a list manually
    triggers_blacklist = {
      -- list of mode / prefixes that should never be hooked by WhichKey
      -- this is mostly relevant for key maps that start with a native binding
      -- most people should not need to change this
      i = { "j", "k" },
      v = { "j", "k" },
    },
  }

  local opts = {
    mode = "n",   -- NORMAL mode
    prefix = "<leader>",
    buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
    silent = true, -- use `silent` when creating keymaps
    noremap = true, -- use `noremap` when creating keymaps
    nowait = true, -- use `nowait` when creating keymaps
  }

  local mappings = {
    ["a"] = { "<cmd>Alpha<cr>", "󰀫  Alpha" },
    ["b"] = {
      "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
      "󰽘  Buffers",
    },
    B = {
      name = " 󰆋  BarBar (Buffer Bar)",
      p = { "<cmd>BufferPick<CR>", "Pick Buffer" },
      P = { "<cmd>BufferPin<CR>", "Pin Buffer" },
      q = { "<cmd>BufferClose<CR>", "Close a Buffer" },
      Q = { "<cmd>BufferCloseAllButPinned<CR>", "Close all Buffers (but Pinned)" },
      r = { "<cmd>BufferRestore<CR>", "Restore Last Closed Buffer" },
      ["1"] = { "<cmd>BufferGoto 1<CR>", "Go to Buffer Tab 1" },
      ["2"] = { "<cmd>BufferGoto 2<CR>", "Go to Buffer Tab 2" },
      ["3"] = { "<cmd>BufferGoto 3<CR>", "Go to Buffer Tab 3" },
      ["4"] = { "<cmd>BufferGoto 4<CR>", "Go to Buffer Tab 4" },
      ["5"] = { "<cmd>BufferGoto 5<CR>", "Go to Buffer Tab 5" },
      ["6"] = { "<cmd>BufferGoto 6<CR>", "Go to Buffer Tab 6" },
      ["7"] = { "<cmd>BufferGoto 7<CR>", "Go to Buffer Tab 7" },
      ["8"] = { "<cmd>BufferGoto 8<CR>", "Go to Buffer Tab 8" },
      ["9"] = { "<cmd>BufferGoto 9<CR>", "Go to Buffer Tab 9" },
      ["0"] = { "<cmd>BufferGoto 10<CR>", "Go to Buffer Tab 10" },
    },
    c = {
      name = " 󰁌  Completion & Snippets",
      e = { '<cmd>lua require("luasnip.loaders").edit_snippet_files()<CR>', "Edit snippets (select)" },
      r = {
        '<cmd>lua require(\'luasnip\').cleanup(); require("luasnip.loaders.from_lua").load({paths = "./luasnip"})<CR>',
        "Reload Lua Snippets",
      },
    },
    d = {
      name = "   Neogen (doc)",
      d = { "<cmd>lua require('neogen').generate()<CR>", "Gen Doc (current scope)" },
      f = { "<cmd>lua require('neogen').generate({ type = 'func' })<CR>", "Document Function" },
      c = { "<cmd>lua require('neogen').generate({ type = 'class' })<CR>", "Document Class" },
      t = { "<cmd>lua require('neogen').generate({ type = 'type' })<CR>", "Document Type" },
      F = { "<cmd>lua require('neogen').generate({ type = 'file' })<CR>", "Docuemnt File" },
    },
    ["e"] = { "<cmd>NvimTreeToggle<cr>", "  Explorer" },
    ["f"] = {
      "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
      "󰭎  Find files",
    },
    ["F"] = {
      "<cmd>Telescope find_files hidden=true<cr>",
      "  Find files (with preview)",
    },
    g = {
      name = " 󰊢  Git",
      b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
      d = { "<cmd>Gitsigns diffthis HEAD<cr>", "Diff" },
      f = { "<cmd>DiffviewFileHistory %<CR>", "File History" },
      g = { "<cmd>Neogit<cr>", "NeoGit ((un)stage/commit/pull/pus)" },
      G = { "<cmd>DiffviewOpen<CR>", "Diffview" },
      h = { "<cmd>GitUIToggle<cr>", "GitUI" },
      j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
      k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
      l = { "<cmd>lua require('FTerm').run('lazygit')<cr>", "LazyGit" },
      L = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
      o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
      p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
      r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
      R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
      s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
      t = { "<cmd>Gitsigns toggle_current_line_blame<cr>", "Toggle Blame (in code)" },
      u = { "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>", "Undo Stage Hunk" },
    },
    ["G"] = { "<cmd>Telescope live_grep_args<cr>", "  Find Text" },
    ["h"] = { "<cmd>nohlsearch<CR>", "󱈍  Remove Search Highlights" },
    l = {
      name = " 󰯂  LSP",
      a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
      c = { "<cmd>RubocopDisableCop<cr>", "Disable Cop (diagnostic)" },
      d = { "<cmd>Telescope lsp_definitions bufnr=0<cr>", "Definitions" },
      D = { "<cmd>Telescope diagnostics bufnr=0<cr>", "Document Diagnostics" },
      w = { "<cmd>Telescope diagnostics<cr>", "Workspace Diagnostics" },
      f = { "<cmd>lua vim.lsp.buf.format{async=true}<cr>", "Format" },
      i = { "<cmd>Telescope lsp_implementations<cr>", "Implementations" },
      I = { "<cmd>LspInfo<cr>", "Info" },
      j = { "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>", "Next Diagnostic" },
      k = { "<cmd>lua vim.lsp.diagnostic.goto_prev()<cr>", "Prev Diagnostic" },
      l = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
      L = { "<cmd>NullLsInfo<cr>", "NullLs Info" },
      m = { "<cmd>Mason<cr>", "Mason" },
      q = { "<cmd>lua vim.diagnostic.setloclist()<cr>", "Quickfix" },
      r = { "<cmd>Telescope lsp_references<cr>", "References" },
      R = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Buffer Rename" },
      t = { "<cmd>:TroubleToggle<cr>", "Toggle Trouble (diagnostics)" },
      s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
      S = { "<cmd>Telescope lsp_workspace_symbols<cr>", "Workspace Symbols" },
    },
    L = {
      name = "   Spell check",
      d = { "<cmd>set spelllang=de<cr>", "German" },
      e = { "<cmd>set spelllang=en<cr>", "English" },
      f = { "<cmd>set spelllang=fr<cr>", "French" },
    },
    m = {
      name = "   Match...",
      w = { "<cmd>lua require('telescope-live-grep-args.shortcuts').grep_word_under_cursor()<cr>", "Match word in code" },
      s = {
        "<cmd>lua require('telescope-live-grep-args.shortcuts').grep_visual_selection()<cr>",
        "Match selection in code"
      },
    },
    o = {
      name = " 󰝰  Open...",
      f = { "<cmd>! xdg-open .<cr>", "Current Folder" },
      g = { "<cmd>exe 'normal \"0yi\"' | exe '! xdg-open https://github.com/'.shellescape(@0, 1)<cr>", "Github Path" },
      u = { "<cmd>normal gx<cr>", "URL" },
    },
    -- jq, yq: detect the file format to pretty print automagically
    p = {
      name = " 󰉊  Pretty Formatting",
      r = { "<cmd>set wrap!<cr>", "Toggle Word Wrap" },
      v = { "<cmd>%s/\\s\\+$/<cr>", "Remove Trailing Whitespaces" },
      w = { "<cmd>hi TrailingWhitespaces ctermbg=red guibg=red<cr>", "Highlight Trailing Whitespaces" },
      W = { "<cmd>hi clear TrailingWhitespace<cr>", "Hide Trailing Whitespaces" },
    },
    ["q"] = { "<cmd>q!<CR>", "󱠡  Quit without saving" },
    ["Q"] = { "<cmd>mksession! ~/.config/nvim/session.vim | qa!<CR>", "󰽂  Save session (not file) and Quit" },
    r = { "<cmd>!cargo run<CR>", "   Run" },
    ["R"] = { "<cmd>lua ReloadConfig()<CR>", "󰑓  Reload Config" },
    ["s"] = { "<cmd>w<CR>", "󰆓  Save Buffer" },
    ["S"] = { "<cmd>wa<CR>", "󰆔  Save All Buffer" },
    t = {
      -- name = " 󰽭  Language Specific Tools",
      name = " 󰽭  Test",
      f = { "<cmd>RailsMinitestRun file<CR>", "Run tests for the current file" },
      j = { "<cmd>RailsMinitestJump<CR>", "Jump to/from test from/to application" },
      l = { "<cmd>RailsMinitestRun line<CR>", "Run the current hovered test" },
    },
    ["T"] = { "<cmd>lua vim.notify('Use ALT-T instead')<CR>", "  Floating Terminal" },
    u = {
      name = "   Debug tools",
      m = { "<cmd>!mpremote a0 run %<CR>", "Upload with mpremote (a0)" },
      n = { "<cmd>!mpremote a1 run %<CR>", "Upload with mpremote (a1)" },
      r = { "<cmd>!cargo espflash flash --monitor --release" },
    },
    U = {
      name = "   Manage Sessions",
      s = { "<cmd>mksession! ~/.config/nvim/session.vim<CR>", "Save Session (to ~/.config/nvim/session.vim)" },
      r = { "<cmd>source ~/.config/nvim/session.vim<CR>", "Restore Session (from ~/.config/nvim/session.vim)" },
    },
    ["w"] = { "<cmd>BufferDelete<CR>", "  Close Buffer" },
    ["W"] = { "<cmd>BufferDelete!<CR>", "! Close Buffer (Force)" },
    z = {
      name = " 󰅩  Syntax",
      c = { "<cmd>set syntax=cucumber<cr>", "Cucumber" },
      j = { "<cmd>set syntax=json<cr>", "JSON" },
      h = { "<cmd>set syntax=html<cr>", "HTML" },
      p = { "<cmd>set syntax=python<cr>", "Python" },
      s = { "<cmd>set syntax=slim<cr>", "Slim" },
      t = { "<cmd>set syntax=javascript<cr>", "Javascript" },
      r = { "<cmd>set syntax=ruby<cr>", "Ruby" },
    },
    Z = {
      name = " 󰭎  Search (Telescope)",
      b = { "<cmd>Telescope buffers<cr>", "Buffers" },
      B = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      c = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
      f = { "<cmd>Telescope find_files<cr>", "Find Files" },
      g = { "<cmd>Telescope live_grep<cr>", "Find Text" },
      h = { "<cmd>Telescope help_tags<cr>", "Find Help" },
      m = {
        "<cmd>lua require('telescope').extensions.media_files.media_files()<cr>",
        "󰭎  Find Medias",
      },
      M = { "<cmd>Telescope man_pages<cr>", "Man Pages" },
      r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
      -- R = { "<cmd>Telescope registers<cr>", "Registers" },
      s = { "<cmd>Telescope lsp_document_symbols<cr>", "Symbols" },
      S = { "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols" },
      k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
      C = { "<cmd>Telescope commands<cr>", "Commands" },
    },
  }

  -- TODO: Adjust "t" to match the current language (filetype)
  -- b = {
  -- 	name = "   Ruby",
  -- 	t = { "<cmd>RoRi18nRenamePath<cr>", "Rename I18n Path" },
  -- },
  -- s = {
  -- 	name = "   Rust",
  -- 	a = { "<cmd>RustCodeAction<cr>", "Code Actions" },
  -- 	c = { "<cmd>RustOpenCargo<cr>", "Open Cargo Toml" },
  -- 	d = { "<cmd>RustDebuggables<cr>", "Debug" },
  -- 	D = { "<cmd>RustLastDebug<cr>", "Last Debug" },
  -- 	h = { "<cmd>RustHoverActions<cr>", "Hover Actions" },
  -- 	j = { "<cmd>RustMoveItemDown<cr>", "Move Item Down" },
  -- 	J = { "<cmd>RustJoinLines<cr>", "Join Lines" },
  -- 	k = { "<cmd>RustMoveItemUp<cr>", "Move Item Up" },
  -- 	r = { "<cmd>RustRun<cr>", "Run" },
  -- 	R = { "<cmd>RustLastRun<cr>", "Last Run" },
  -- },

  which_key.setup(setup)
  which_key.register(mappings, opts)
end
