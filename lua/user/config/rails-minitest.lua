return {
  terminal_command = "gnome-terminal -- zsh -ic \"cd . && %CMD% && zsh\"",
  fterm_enabled = true
}
