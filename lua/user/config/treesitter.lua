return {
  ensure_installed = {
    "ruby",
    "bash",
    "c",
    "javascript",
    "json",
    "lua",
    "python",
    "css",
    "rust",
    "yaml",
    "markdown",
    "markdown_inline",
    "toml",
  },
  sync_install = false,
  auto_install = true,
  ignore_install = { "phpdoc" },
  highlight = {
    enable = true,
    disable = { "css", "javascript" },
    additional_vim_regex_highlighting = false
  },
  indent = { enable = true, disable = { "python", "css", "ruby" } },
}
