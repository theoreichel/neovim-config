return {
  enable = true,
  snippet_engine = "luasnip",
  languages = {
    ruby = {
      template = {
        annotation_convention = "yard",
        -- yard = {
        --   { nil,                "# $1",                { no_results = true, type = { "class", "func" } } },
        --   { nil,                "# $1",                { no_results = true, type = { "type" } } },
        --   { nil,                "# $1" },
        --   { "parameters",       "# @param %s [$1] $1", {} },
        --   { "return_statement", "# @return [$1] $1",   {} },
        -- }
      },
      placeholders_text = {
        ["description"] = "<description>",
        ["tparam"] = "<tparam>",
        ["parameter"] = "<parameter>",
        ["return"] = "<return>",
        ["class"] = "<class>",
        ["throw"] = "<throw>",
        ["varargs"] = "<varargs>",
        ["type"] = "<type>",
        ["attribute"] = "<attribute>",
        ["args"] = "<args>",
        ["kwargs"] = "<kwargs>",
      },
    },
  },
}
