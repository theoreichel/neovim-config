return {
  projects = { -- define project roots
    "~/Code/*",
    "~/Code/nvim-plugins/*",
    "~/Seafile/Code/*",
    "~/Seafile/Stocubo/cluster",
  },
  last_session_on_startup = true
}
