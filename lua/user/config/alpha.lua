return function ()
  alpha = require("alpha")

  local dashboard = require("alpha.themes.dashboard")

  -- dashboard.section.header.val = {
  -- ASCII ART
  -- }

  dashboard.section.buttons.val = {
    dashboard.button("o", "󰈞  Recent Projects", ":Telescope neovim-project history<CR>"),
    dashboard.button("d", "󰈞  Discover Projects", ":Telescope neovim-project discover<CR>"),
    dashboard.button("f", "󰈞  Find file", ":Telescope find_files <CR>"),
    dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),
    dashboard.button("r", "󰦛  Recently used files", ":Telescope oldfiles <CR>"),
    dashboard.button("R", "  Restore last session", ":source ~/.config/nvim/session.vim <CR>"),
    dashboard.button("c", "  Configuration", ":cd ~/.config/nvim | e $MYVIMRC <CR>"),
    dashboard.button("q", "󰠚  Quit Neovim", "<cmd>qa<CR>"),
  }

  local function footer()
    -- NOTE: requires the fortune-mod package to work
    local handle = io.popen("fortune")
    if handle then
      local fortune = handle:read("*a")
      if fortune then
        handle:close()
        return "\n" .. fortune
      end
    end
    return "No Fortune today."
  end

  dashboard.section.footer.val = footer()

  dashboard.section.footer.opts.hl = "Text"
  dashboard.section.header.opts.hl = "Include"
  dashboard.section.buttons.opts.hl = "Keyword"

  dashboard.opts.opts.noautocmd = true
  -- vim.cmd([[autocmd User AlphaReady echo 'ready']])

  alpha.setup(dashboard.opts)
end
