local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet
local k = require("luasnip.nodes.key_indexer").new_key

---------------
-- Settings ---
---------------
RUBY_TYPES = {
  'Integer',
  'String',
  'Boolean',
  'NilClass',
  'Hash',
  'Array',
  'Object'
}

RESPONSE_FORMATS = {
  'html',
  'json',
  'turbo_stream',
  'xml',
  'csv',
  'js'
}

-- enable debugging
-- ls.log.set_loglevel('debug')

----------------
-- Templates ---
----------------

-- TODO: Class with Model/Controller/... Inheritance
-- local class_template = [[
-- # frozen_string_literal: true
--
-- #
-- # Class {}
-- #
-- # @author {}
-- #
-- class {} < {}
--   {}
--   {}
-- end
-- ]]

-- TODO
-- local relationship_template = [[
-- ]]

-- local validation_template = [[
-- ]]

-- local scope_template = [[
-- ]]

local simple_function_template = [[
#
# {}
#{}
# @return [{}] {}
#
def {}({})
  {}
end
]]

local rescue_function_template = [[
#
# {}
#{}
# @return [{}] {}
#
def {}({})
  {}
rescue {} => e
  {}
ensure
  {}
end
]]

local one_line_function_template = [[
#
# {}
#{}
# @return [{}] {}
#
def {}({}) = {}
]]

local ror_format_template = "format.{} {{ {} }}"
local ror_respond_to_template = "respond_to do |format|\n\t" .. ror_format_template .. "\nend"


---------------------
-- Build Snippets ---
---------------------

-- Nodes cannot be reused, they must be created for each application.
local function build_text_nodes_list(list)
  local text_nodes = {}
  for key, val in pairs(list) do text_nodes[key] = t(val) end
  return text_nodes
end

local function strip(text)
  return text:match("^%s*(.-)%s*$")
end

local function generate_comments(values)
  local param_str = values[1][1]
  -- return nothing if no parameter is provided
  if param_str:match("^%s+$") or param_str == '' then
    return sn(1, { t('') })
  end

  local params = vim.split(param_str, ',')
  local nodes = {}

  for index, param in ipairs(params) do
    -- split key values pairs, if any
    local key_value = vim.split(param, '=')
    local param_description = 'description'
    if #key_value == 2 then
      param_description = '(default: ' .. strip(key_value[2]) .. ')'
    end

    local key = strip(key_value[1])
    -- sometimes a ) is added to the last parameter, gsub will remove it.
    -- (if the closing parenthesis has been removed by a plugin and added by the user)
    key = key:gsub("%)", '')

    table.insert(
      nodes,
      sn(
        index,
        fmt('\n\n# @param {} [{}] {}', {
          t(key),
          -- TODO: detect the object class
          c(1, build_text_nodes_list(RUBY_TYPES)),
          i(2, param_description)
        })
      )
    )
  end

  table.insert(nodes, sn(nil, fmt('\n\n#', {})))
  return sn(nil, nodes)
end

local function ror_format_definition()
  return {
    -- formats available to respond
    c(1, build_text_nodes_list(RESPONSE_FORMATS)),
    i(2, 'render')
  }
end

-- TODO: keep values like function name, params from a "choice" to another.
ls.add_snippets('ruby', {
  ----------------
  -- Classes ---
  ----------------
  -- TODO

  ----------------
  -- Functions ---
  ----------------
  s('def',
    c(
      1,
      {
        fmt(
          simple_function_template,
          {
            -- documentation: description
            i(1, 'Function description'),
            -- documentation: dynamically generated params list
            d(4, generate_comments, { 3 }),
            -- documentation: returned type
            c(6, build_text_nodes_list(RUBY_TYPES)),
            -- documentation: description of the function output
            i(7, 'description'),
            -- function name
            i(2, 'name'),
            -- parameters
            i(3),
            -- function body
            i(5, 'return')
          }
        ),
        fmt(
          rescue_function_template,
          {
            -- documentation: description
            i(1, 'Function description'),
            -- documentation: dynamically generated params list
            d(4, generate_comments, { 3 }),
            -- documentation: returned type
            c(9, build_text_nodes_list(RUBY_TYPES)),
            -- documentation: description of the function output
            i(10, 'description'),
            -- function name
            i(2, 'name'),
            -- parameters
            i(3),
            -- function body
            i(5, 'return'),
            -- error catching: error class
            i(6, 'StandardError'),
            -- error catching: cateched error body
            i(7, 'raise e'),
            -- error catching: ensure body
            i(8, 'cleanup'),
          }
        ),
        fmt(
          one_line_function_template,
          {
            -- documentation: description
            i(1, 'Function description'),
            -- documentation: dynamically generated params list
            d(4, generate_comments, { 3 }),
            -- documentation: returned type
            c(6, build_text_nodes_list(RUBY_TYPES)),
            -- documentation: description of the function output
            i(7, 'description'),
            -- function name
            i(2, 'name'),
            -- parameters
            i(3),
            -- function body
            i(5, 'return')
          }
        ),
      }
    )
  ),
  ------------------
  -- ROR methods ---
  ------------------
  s('respond_to',
    fmt(
      ror_respond_to_template,
      ror_format_definition()
    )
  ),
  s('format',
    fmt(
      ror_format_template,
      ror_format_definition()
    )
  )
})
