require "user.autocommands"
require "user.filetypes"
require "user.keymaps"
require "user.options"

-- Bootstrap lazy.nvim if not yet installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Use a protected call so we don't error out on first use
local status_ok, lazy = pcall(require, "lazy")
if not status_ok then
  vim.notify("Unable to load lazy", vim.log.levels.ERROR)
  return
end

lazy.setup(
  require("user.plugins"),
  require("user.config.lazy")
)

-- Colorscheme
local variants = { dark = "nightfox", light = "dawnfox" }
-- Apply the system appearance preferences (dark/light) to the nightfox theme.
-- TODO: change the colorscheme live when settings are changed in the system
local theme_variant = ""
local system_theme = vim.api.nvim_exec("!gsettings get org.gnome.desktop.interface color-scheme", true)
if string.match(system_theme, "prefer.dark") then
  theme_variant = variants["dark"]
else
  theme_variant = variants["light"]
end

-- Load the theme (colorscheme), calling "colorscheme" will lazy-load the theme
local colorscheme_ok = pcall(vim.cmd, "colorscheme " .. theme_variant)
if not colorscheme_ok then
  vim.notify("Unable to load colorscheme " .. theme_variant, vim.log.levels.WARN)
  return
end

-- Keep it? Currently too buggy to be used
-- require "user.utils"

-- Override settings for Neovide, if this is the current editor
require "user.neovide"
